Updating the .make files for CiviCRM Starter Kit Distribution (deprecated)
==========================================================================

The CiviCRM Starter Kit starts with a stripped down version of CiviCRM that is built with each release of CiviCRM as part of the CiviCRM team's packaging process.

http://downloads.civicrm.org/civicrm-x.x.x-starterkit.tgz where x.x.x would be 4.3.3 or the current release. You can see the version of CiviCRM that's currently being packaged in http://drupalcode.org/project/civicrm_starterkit.git/blob/refs/heads/7.x-4.x:/civicrm_starterkit.make

## Patches to Add Libraries and Files Back to CiviCRM
http://drupalcode.org/project/civicrm_starterkit.git/blob/refs/heads/7.x-4.x:/civicrm_starterkit.make#l36

## Patches Related to Running CiviCRM from profiles/
http://drupalcode.org/project/civicrm_starterkit.git/blob/refs/heads/7.x-4.x:/civicrm_starterkit.make#l32

CiviCRM has normally be limited to running from sites/all/modules or site/all/modules/contrib

## Pantheon Specific Patches

http://drupalcode.org/project/civicrm_starterkit.git/blob/refs/heads/7.x-4.x:/civicrm_starterkit.make#l27

The biggest changes related to Pantheon are related to the template used to generate the civicrm.settings.php file. Because the same file moves from dev -> test -> live, we can't use hardcoded values. Because we want to be able to use the same codebase when we're not using Pantheon, that part of the code checks to see `$_SERVER['PRESSFLOW_SETTINGS']` is set. If it is, the normal CiviCRM settings are ignored. A comment about that is added to the civicrm.settings.php to make this less confusing for for the Pantheon and non-Pantheon users.

// PANTHEON USERS - These settings are overridden above when running on Pantheon.
// These settings are ONLY included here to remain compatible all other hosts.

`define( 'CIVICRM_UF_DSN'           , 'mysql://username:pass@localhost/drupal?new_link=true' );`

##Drush Make

drush verify-makefile drupal-org.make

##Process for Testing an Update
###Process for Testing an Update on Pantheon

1. Correct the problem with a patch on Drupal.org
2. Apply the patch using the drupal-org.make
3. Submit the patch to CiviCRM's Jira instance
4. When the patch is committed by the CiviCRM core team, Jira is updated
5. When the next release of CiviCRM is rolled, I remove any patches from the drupal-org.make that have been fixed in CiviCRM
6. Package a new release of http://drupal.org/project/civicrm_starterkit
7. Replace the profiles/civicrm_starterkit in https://github.com/kreynen/civicrm-starterkit-drops-7 with the packaged version downloaded from Drupal.org
8. Commit the changes
9. Watch for update in Pantheon Dashboard
10. Apply update
11. If the changes are in the install process, test a new install
